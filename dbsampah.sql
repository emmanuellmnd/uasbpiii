-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 17, 2020 at 02:29 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `dbsampah`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_petugas`
--

CREATE TABLE IF NOT EXISTS `detail_petugas` (
  `dtl_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `id_rute` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `status_rute` enum('Pilih','Selesai') NOT NULL,
  `nama_rute` varchar(10) NOT NULL,
  PRIMARY KEY (`dtl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `detail_petugas`
--

INSERT INTO `detail_petugas` (`dtl_id`, `usr_id`, `id_rute`, `nama`, `status_rute`, `nama_rute`) VALUES
(1, 2, 2, 'Joko Anwar B', 'Pilih', 'RUTE B');

-- --------------------------------------------------------

--
-- Table structure for table `detail_rute`
--

CREATE TABLE IF NOT EXISTS `detail_rute` (
  `id_rute` int(11) NOT NULL AUTO_INCREMENT,
  `nama_rute` varchar(30) NOT NULL,
  `detail_rute` text NOT NULL,
  PRIMARY KEY (`id_rute`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `detail_rute`
--

INSERT INTO `detail_rute` (`id_rute`, `nama_rute`, `detail_rute`) VALUES
(1, 'RUTE A', 'Jalan Kesana Kemari'),
(2, 'RUTE B', 'Jalan Kanan'),
(3, 'RUTE C', 'asdajshdkjahskjdhaksd'),
(14, 'RUTE D', 'Aku nak ingin\r\nwkwkwk\r\nwkwkwkkw\r\nwkwkwk');

-- --------------------------------------------------------

--
-- Table structure for table `detail_warga`
--

CREATE TABLE IF NOT EXISTS `detail_warga` (
  `dtl_id` int(11) NOT NULL AUTO_INCREMENT,
  `usr_id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(150) NOT NULL,
  `status` enum('Kosong','Penuh') NOT NULL,
  PRIMARY KEY (`dtl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `detail_warga`
--

INSERT INTO `detail_warga` (`dtl_id`, `usr_id`, `nama`, `alamat`, `status`) VALUES
(1, 1, 'Budi Santosa', 'Perum Lojiwetan no 4 Solobaru\n\nhjkhgh', 'Penuh'),
(5, 5, 'Donny Grager', 'Perum Zelatan no 11 Soloo', 'Kosong');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `usr_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `level` enum('Administrator','Petugas','Warga') NOT NULL,
  PRIMARY KEY (`usr_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`usr_id`, `username`, `password`, `level`) VALUES
(1, 'budisan', 'budisan', 'Warga'),
(2, 'petugas', 'petugas', 'Petugas'),
(5, 'donny', 'donny', 'Warga'),
(8, 'admin', 'admin', 'Administrator');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
