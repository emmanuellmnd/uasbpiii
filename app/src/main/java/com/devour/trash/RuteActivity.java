package com.devour.trash;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.devour.trash.adapter.AdapterRute;
import com.devour.trash.adapter.DataRute;
import com.devour.trash.app.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RuteActivity extends AppCompatActivity {


    ProgressDialog pDialog;

    RecyclerView mRecyclerview;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mManager;
    List<DataRute> mItems;


    int success;
    View dialogView;
    EditText txt_id_rute, txt_nama_rute, txt_alamat_rute;
    String id_rute, nama_rute, detail_rute, usr_id;

    private static final String TAG = RuteActivity.class.getSimpleName();

    private static String url     = Server.URL + "data_rute.php";


    public static final String TAG_USR_ID       = "usr_id";
    public static final String TAG_ID_RUTE       = "id_rute";
    public static final String TAG_NAMA_RUTE     = "nama_rute";
    public static final String TAG_DETAIL_RUTE   = "detail_rute";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    String tag_json_obj = "json_obj_req";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rute);

        usr_id = getIntent().getStringExtra(TAG_USR_ID);

        mRecyclerview = findViewById(R.id.list);
        mItems = new ArrayList<>();
        callRute();
        mManager = new LinearLayoutManager(RuteActivity.this, LinearLayoutManager.VERTICAL,false);
        mRecyclerview.setLayoutManager(mManager);
        mAdapter = new AdapterRute(RuteActivity.this, mItems);
        mRecyclerview.setAdapter(mAdapter);

    }

    private void callRute() {
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setMessage("Data Rute ...");
        showDialog();

        JsonArrayRequest reqData = new JsonArrayRequest(Request.Method.POST, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e(TAG, "Rute Response: " + response.toString());
                hideDialog();
                for(int i = 0; i<response.length(); i++)
                {
                try {
                    // Check for error node in json
                    JSONObject data = response.getJSONObject(i);
                    DataRute dataRute = new DataRute();
                    dataRute.setUsr_id(usr_id);
                    dataRute.setId_rute(data.getString("id_rute"));
                    dataRute.setNama_rute(data.getString("nama_rute"));
                    dataRute.setDetail_rute(data.getString("detail_rute"));
                    mItems.add(dataRute);
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }
            }
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Data Rute error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        });
        AppController.getInstance().addToRequestQueue(reqData);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
