package com.devour.trash.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.devour.trash.DataActivity;
import com.devour.trash.MainActivity;
import com.devour.trash.R;
import com.devour.trash.Server;
import com.devour.trash.app.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdapterRute extends RecyclerView.Adapter<AdapterRute.HolderData> {
    private List<DataRute> mItems;
    private Context context;
    private Dialog mDialog;
    ProgressDialog pDialog;

    int success;

    String tag_json_obj = "json_obj_req";

    private static final String TAG = AdapterRute.class.getSimpleName();

    public static final String TAG_STATUS_RUTE = "status_rute";
    public static final String TAG_ID_RUTE = "id_rute";
    public static final String TAG_USR_ID = "usr_id";
    public static final String TAG_NAMA_RUTE = "nama_rute";

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private String url = Server.URL + "update_status_rute.php";

    public AdapterRute (Context context, List<DataRute> items)
    {
        this.mItems = items;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderData onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_rute_row, parent, false);
        final HolderData holderData = new HolderData(layout);

        mDialog = new Dialog(context);
        mDialog.setContentView(R.layout.content_rute);

        holderData.list_rute_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView dialog_nama_rute = mDialog.findViewById(R.id.tv_nama_rute);
                TextView dialog_detail_rute = mDialog.findViewById(R.id.tv_detail_rute);
                TextView dialog_id = mDialog.findViewById(R.id.tv_id);
                String status_rute,id_rute, usr_id;
                Button btn_batal = mDialog.findViewById(R.id.btn_batal);
                Button btn_pilih = mDialog.findViewById(R.id.btn_pilih);


                dialog_id.setText(mItems.get(holderData.getAdapterPosition()).getUsr_id());
                dialog_nama_rute.setText(mItems.get(holderData.getAdapterPosition()).getNama_rute());
                dialog_detail_rute.setText(mItems.get(holderData.getAdapterPosition()).getDetail_rute());


                Toast.makeText(context, "Melihat detail " +String.valueOf(mItems.get(holderData.getAdapterPosition()).getNama_rute()), Toast.LENGTH_SHORT).show();
                mDialog.show();

                btn_batal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "Batal memilih " +String.valueOf(mItems.get(holderData.getAdapterPosition()).getNama_rute()), Toast.LENGTH_SHORT).show();
                        mDialog.dismiss();
                    }
                });

                btn_pilih.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String usr_id = mItems.get(holderData.getAdapterPosition()).getUsr_id();
                        String id_rute = mItems.get(holderData.getAdapterPosition()).getId_rute();
                        String nama_rute = mItems.get(holderData.getAdapterPosition()).getNama_rute();
                        String status_rute = "Pilih";
                        Toast.makeText(context, "Memilih " +String.valueOf(mItems.get(holderData.getAdapterPosition()).getNama_rute()), Toast.LENGTH_SHORT).show();

                        updateStatusRute(usr_id,id_rute,status_rute,nama_rute);

                    }
                });
            }
        });

        return holderData;
    }

    private void updateStatusRute(final String usr_id, final String id_rute, final String status_rute,final String nama_rute) {
        pDialog = new ProgressDialog(context);
        pDialog.setCancelable(false);
        pDialog.setMessage("Memilih ...");
        showDialog();

        StringRequest strReq = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Memilih Rute Response: " + response.toString());
                hideDialog();
                try {
                    JSONObject jObj = new JSONObject(response);
                    success = jObj.getInt(TAG_SUCCESS);

                    // Check for error node in json
                    if (success == 1) {


                        Log.e("Successfully Edit Data!", jObj.toString());

                        Toast.makeText(context.getApplicationContext(), jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                        mDialog.dismiss();

                    } else {
                        Toast.makeText(context.getApplicationContext(),
                                jObj.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Memilih Rute error: " + error.getMessage());
                Toast.makeText(context.getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();

                hideDialog();

            }
        }){

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("usr_id", usr_id);
                params.put("id_rute", id_rute);
                params.put("status_rute", status_rute);
                params.put("nama_rute", nama_rute);
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(strReq, tag_json_obj);;

    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    @Override
    public void onBindViewHolder(@NonNull HolderData holder, int position) {
        DataRute dataRute = mItems.get(position);
        holder.tvid_rute.setText(dataRute.getId_rute());
        holder.tvnama_rute.setText(dataRute.getNama_rute());
        holder.tvdetail_rute.setText(dataRute.getDetail_rute());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    class HolderData extends  RecyclerView.ViewHolder
    {
        TextView tvid_rute,tvnama_rute, tvdetail_rute;
        LinearLayout list_rute_row;
        public  HolderData(View view)
        {
            super(view);
            list_rute_row = view.findViewById(R.id.ll);
            tvid_rute = view.findViewById(R.id.txt_id_rute);
            tvnama_rute = view.findViewById(R.id.txt_nama_rute);
            tvdetail_rute = view.findViewById(R.id.txt_detail_rute);
        }
    }

}
